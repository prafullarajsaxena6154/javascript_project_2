const test_Defaults = (obj , defaultProps) =>
{
    for(let key in defaultProps)
    {
        if(!(key in obj))
        {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
};

module.exports = test_Defaults;