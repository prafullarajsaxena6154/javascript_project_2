const test_values = (obj) =>
{
    let value_Arr = [];
    for(let key in obj)
    {
        if(!(typeof(obj[key]) === 'function'))
        {
            value_Arr.push(obj[key]);
        }
    }
    return value_Arr;
};

module.exports = test_values;