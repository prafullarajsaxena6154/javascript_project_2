const test_Invert = (obj) =>
{
    let invert_Obj = {};
    for(let key in obj)
    {
        invert_Obj[obj[key]] = key;
    }
    return invert_Obj;
};
module.exports = test_Invert;