const test_Pairs = (obj) =>
{
    let pair_Arr = [];
    for(let key in obj)
    {
        if(!(typeof(obj[key]) === 'function'))
        {
            pair_Arr.push(key , obj[key]);
        }
    }
    return pair_Arr;
};

module.exports = test_Pairs;