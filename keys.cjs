const test_Keys = (obj) =>{
    let key_Arr = [];
        for(let key in obj)
        {
            if(typeof key === 'string')
            {
                key_Arr.push(key);
            }
            else
            {
                key_Arr.push(JSON.stringify(key));
            }
        }
        return key_Arr;
    };

module.exports = test_Keys;