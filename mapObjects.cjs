const test_Map_Objects = (obj, map_Obj_Func) =>
{
    for(let key in obj)
    {
        obj[key]= map_Obj_Func(obj[key], key);
    }
    return obj;
};

module.exports = test_Map_Objects;